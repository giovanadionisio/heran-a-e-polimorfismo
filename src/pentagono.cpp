#include "pentagono.hpp"

Pentagono::Pentagono(){
    setBase(1.0);
    setAltura(1.0);
    setTipo("Pentagono");
}

Pentagono::Pentagono(float lado, float apotema, string tipo){
    setBase(lado);
    setAltura(apotema);
    setTipo(tipo);
}

float Pentagono::CalculaArea(){
    float area;

    area = (CalculaPerimetro() * getAltura())/2.0;
    return area;
}

float Pentagono::CalculaPerimetro(){
    float perimetro;

    perimetro = 5 * getBase();
    return perimetro;
}