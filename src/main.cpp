#include "formageometrica.hpp"
#include "quadrado.hpp"
#include "triangulo.hpp"
#include "circulo.hpp"
#include "paralelogramo.hpp"
#include "pentagono.hpp"
#include "hexagono.hpp"

int main(){
    Quadrado quadrado;
    Triangulo triangulo;
    Circulo circulo;
    Paralelogramo paralelogramo;
    Pentagono pentagono;
    Hexagono hexagono;

    cout << quadrado.getTipo() << " Area: " << quadrado.CalculaArea() << " Perimetro: " << quadrado.CalculaPerimetro() << endl;
    cout << triangulo.getTipo() << " Area: " << triangulo.CalculaArea() << " Perimetro: " << triangulo.CalculaPerimetro() << endl;
    cout << circulo.getTipo() << " Area: " << circulo.CalculaArea() << " Perimetro: " << circulo.CalculaPerimetro() << endl;
    cout << paralelogramo.getTipo() << " Area: " << paralelogramo.CalculaArea() << " Perimetro: " << paralelogramo.CalculaPerimetro() << endl;
    cout << pentagono.getTipo() << " Area: " << pentagono.CalculaArea() << " Perimetro: " << pentagono.CalculaPerimetro() << endl;
    cout << hexagono.getTipo() << " Area: " << hexagono.CalculaArea() << " Perimetro: " << hexagono.CalculaPerimetro() << endl;

    
    
    
    
    return 0;
}