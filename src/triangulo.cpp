#include "triangulo.hpp"

Triangulo::Triangulo(){
    setBase(1.0);
    setAltura(1.0);
    setTipo("Triângulo");
}

Triangulo::Triangulo(float base, float altura, string tipo){
        setBase(base);
        setAltura(altura);
        setTipo(tipo);
}

float Triangulo::CalculaArea(){
    float area;
    
    area = (getBase() * getAltura())/2;
    return area;
}

float Triangulo::CalculaPerimetro(){
    float perimetro;

    perimetro = sqrt((getBase()*getBase())+(getAltura()*getAltura())) + getAltura() + getBase();
    return perimetro;
}
